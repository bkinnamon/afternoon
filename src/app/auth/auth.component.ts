import {
  Component,
  Inject,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
} from '@angular/core';
import { Router } from '@angular/router';
import { isPlatformServer } from '@angular/common';
import { Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { trace } from '@angular/fire/performance';
import firebase from 'firebase/app';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit, OnDestroy {
  private readonly userDisposable?: Subscription;
  showLoginButton = false;
  showLogoutButton = false;

  constructor(
    private auth: AngularFireAuth,
    @Inject(PLATFORM_ID) platformId: object,
    private router: Router
  ) {
    if (!isPlatformServer(platformId)) {
      this.userDisposable = this.auth.authState
        .pipe(
          trace('auth'),
          map((u) => !!u)
        )
        .subscribe((isLoggedIn) => {
          this.showLoginButton = !isLoggedIn;
          this.showLogoutButton = isLoggedIn;
        });
    }
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    if (this.userDisposable) {
      this.userDisposable.unsubscribe();
    }
  }

  login() {
    this.auth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(() => {
        this.router.navigate(['projects']);
      });
  }

  logout() {
    this.auth.signOut().then(() => {
      this.router.navigate(['']);
    });
  }
}

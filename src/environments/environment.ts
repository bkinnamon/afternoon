// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBFPc_6U2fSmH1AZ-7K-e4eonIqR3kqgsk',
    authDomain: 'brettk-afternoon.firebaseapp.com',
    databaseURL: 'https://brettk-afternoon.firebaseio.com',
    projectId: 'brettk-afternoon',
    storageBucket: 'brettk-afternoon.appspot.com',
    messagingSenderId: '267195946532',
    appId: '1:267195946532:web:3cb0f3c1c3f175afbeaab1',
    measurementId: 'G-W5L8WSMJDV',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
